#!/bin/bash

source .version
SUFFIX="$1"
if [ -z $SUFFIX ] || [[ "$SUFFIX" == "" ]]; then
    VERSION_TAG="$VERSION"
else
    VERSION_TAG="$VERSION-$SUFFIX"
fi

docker build -t $IMAGE_NAME:$VERSION_TAG -f docker/Dockerfile .
docker tag $IMAGE_NAME:$VERSION_TAG $IMAGE_NAME:latest
