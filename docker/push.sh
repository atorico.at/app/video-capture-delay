#!/bin/bash

source .version
SUFFIX="$1"
if [ -z $SUFFIX ] || [[ "$SUFFIX" == "" ]]; then
    VERSION_TAG="$VERSION"
else
    VERSION_TAG="$VERSION-$SUFFIX"
fi

docker push $IMAGE_NAME:latest
docker push $IMAGE_NAME:$VERSION_TAG
