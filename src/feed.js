const startRecording = document.getElementById('startRecording');
const saveRecording = document.getElementById('saveRecording');

saveRecording.disabled = true;

let mediaRecorder;
let recordedChunks = [];

const downloadName = () => {
    const date = new Date();
    return date.toISOString().replace(/:/g, '-').split('.')[0];
}

startRecording.addEventListener('click', () => {
    startRecording.disabled = true;
    saveRecording.disabled = false;
    const canvas = document.getElementById('delayedCanvas');
    const stream = canvas.captureStream(30); // 30 FPS
    mediaRecorder = new MediaRecorder(stream);
    mediaRecorder.ondataavailable = handleDataAvailable;
    mediaRecorder.start();

    function handleDataAvailable(event) {
        if (event.data.size > 0) {
            const url = URL.createObjectURL(event.data);
            const a = document.createElement('a');
            document.body.appendChild(a);
            a.style = 'display: none';
            a.href = url;
            a.download = 'video-delay-' + downloadName() + '.webm';
            notify(a.download);
            a.click();
            window.URL.revokeObjectURL(url);
        }
    }
});
saveRecording.addEventListener('click', () => {
    try {
        startRecording.disabled = false;
        saveRecording.disabled = true;
        mediaRecorder.stop();
    } catch (e) {
        console.error(e);
    }
});

function notify(name) {
    const msg = "Downloading video: " + name;
    if (!("Notification" in window)) {
        // Check if the browser supports notifications
        console.log(msg);
    } else if (Notification.permission === "granted") {
        // Check whether notification permissions have already been granted;
        // if so, create a notification
        navigator.serviceWorker.ready.then((registration) => {
            registration.showNotification(msg);
        });
    } else if (Notification.permission !== "denied") {
        // We need to ask the user for permission
        Notification.requestPermission().then((permission) => {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
                navigator.serviceWorker.ready.then((registration) => {
                    registration.showNotification(msg);
                });
            }
        });
    } else {
        console.log(msg);
    }
}
