const videoContainer = document.getElementById('videoContainer');
const videoElement = document.getElementById('cameraFeed');
const delayedVideo = document.getElementById('delayedVideo'); // This should be another <img> or <canvas> element in your HTML
const delayInput = document.getElementById('delayInput');
const frameRateInput = document.getElementById('frameRateInput');
const selectUserCamera = document.getElementById('selectUserCamera');
const selectEnvironmentCamera = document.getElementById('selectEnvironmentCamera');

const frameRate = localStorage.getItem("video.delay.frameRate") || 20;
const delayInSeconds = localStorage.getItem("video.delay.seconds") || 25;
let frameBuffer = [];


const start = Date.now();

const camera = localStorage.getItem("video.delay.camera") || "environment";
if (camera === "user") {
    selectUserCamera.checked = true;
} else {
    selectEnvironmentCamera.checked = true;
}
delayInput.value = delayInSeconds;
frameRateInput.value = frameRate;


function captureFrame() {
    const canvas = document.createElement('canvas');
    canvas.setAttribute('id', 'canvas');
    const ctx = canvas.getContext('2d');
    canvas.width = videoElement.videoWidth;
    canvas.height = videoElement.videoHeight;
    ctx.drawImage(videoElement, 0, 0, canvas.width, canvas.height);
    const imageDataUrl = canvas.toDataURL();

    frameBuffer.push(imageDataUrl);

    const counter = diffInSeconds(start, Date.now());
    if (counter > delayInSeconds) {
        const delayedVideo = document.createElement('img'); // This should be another <img> or <canvas> element in your HTML
        const frameToDisplay = frameBuffer.shift();
        delayedVideo.src = frameToDisplay;
        delayedVideo.onload = () => {
            const delayedCanvas = document.getElementById('delayedCanvas');
            const delayedCanvasCtx = delayedCanvas.getContext('2d');
            delayedCanvasCtx.drawImage(videoElement, 0, 0, delayedCanvas.width, delayedCanvas.height);

        };
    }
}

navigator.mediaDevices.enumerateDevices()
    .then(function(devices) {
        const videoDevices = devices.filter(device => device.kind === 'videoinput');
        if (videoDevices.length > 1) {

            return navigator.mediaDevices.getUserMedia({
                video: {
                    facingMode: camera
                } // Attempts to select the rear camera
            });
        } else if (videoDevices.length === 1) {
            // If there's only one camera, just use it
            return navigator.mediaDevices.getUserMedia({
                video: true
            });
        } else {
            throw new Error('No video input devices found');

        }
    })
    .then(function(stream) {
        // Use the stream as before
        videoElement.srcObject = stream;
        videoElement.play();

        console.log("Time: " + Number(1000 / frameRate).toFixed(0));
        setInterval(captureFrame, Number(1000 / frameRate).toFixed(0));
    })
    .catch(function(error) {
        console.error('Error accessing the camera:', error);
    });


toggleFullScreen = (e) => {
    const fsElement = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement;
    if (fsElement) {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    } else {
        const vc = e.currentTarget;
        if (vc.requestFullscreen) {
            vc.requestFullscreen();
        } else if (vc.mozRequestFullScreen) { /* Firefox */
            vc.mozRequestFullScreen();
        } else if (vc.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
            vc.webkitRequestFullscreen();
        } else if (vc.msRequestFullscreen) { /* IE/Edge */
            vc.msRequestFullscreen();
        }
    }
}

// Fullscreen toggle on video click
videoContainer.addEventListener('click', toggleFullScreen);

document.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('form');
    const delayInput = document.getElementById('delayInput');
    const frameRateInput = document.getElementById('frameRateInput');
    const selectUserCamera = document.getElementById('selectUserCamera');
    const selectEnvironmentCamera = document.getElementById('selectEnvironmentCamera');

    form.addEventListener('submit', function(event) {
        event.preventDefault(); // Prevent the form from submitting in the traditional way

        const delayInSeconds = parseInt(delayInput.value, 10); // Convert the input value to an integer
        const frameRate = parseInt(frameRateInput.value, 10); // Convert the input value to an integer
        const userCamera = selectUserCamera.checked;
        const environmentCamera = selectEnvironmentCamera.checked;

        if (isNaN(delayInSeconds)) {
            alert('Please enter a valid number');
            return;
        }


        if (isNaN(frameRate)) {
            alert('Please enter a valid number');
            return;
        }

        console.log(`Setting delay to ${delayInSeconds} seconds.`);
        localStorage.setItem("video.delay.seconds", delayInSeconds);

        console.log(`Setting frameRate to ${frameRate} per second.`);
        localStorage.setItem("video.delay.frameRate", frameRate);

        if (userCamera) {
            console.log(`Setting Camera to Front Facing Camera.`);
            localStorage.setItem("video.delay.camera", "user");
        } else {
            console.log(`Setting Camera to Environment Facing Camera.`);
            localStorage.setItem("video.delay.camera", "environment");
        }

        window.location.reload();
    });
});

const diffInSeconds = (start, end) => {
    const diff = end - start;
    return Math.floor(diff / 1000);
}

document.addEventListener('DOMContentLoaded', () => {
    let counter = 0;
    setInterval(() => {
        counter = diffInSeconds(start, Date.now());
        const counterValue = counter;
        document.getElementById('counterValue').textContent = counterValue;
    }, 1000); // Increment the counter every second
});
